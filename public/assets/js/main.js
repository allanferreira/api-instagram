$(function(){
	init();

	$('.filter-request select').change(function() {
		var v = $(this).val();
	
		switch(v) {
			case 'start':
				$( ".temp .item" ).each(function( index ) {
					$(this).addClass('animated zoomOutLeft');
				});	
				$( ".case .item" ).each(function( index ) {
					$(this).attr('class','');
					$(this).addClass('animated bounceInUp item');
				});	

				break;
			case 'all':
				$('.temp').html('');
				$( ".temp .item" ).each(function( index ) {
					$(this).addClass('animated zoomOutLeft');
				});	
				$( ".case .item" ).each(function( index ) {
					var item = $(this).html();
					$(this).attr('class','');
					$(this).addClass('animated bounceOutUp item');
					$('.temp').append('<div class="col span_1_of_3 animated bounceInDown"><div class="item">'+item+'</div></div>');
				});				      
				break;
			case 'likes':
			   var likes = [];
			   
			    $( ".case .item" ).each(function( index ) {
			        like = $(this).find('img').data('likes');
			        n = parseInt(like);
			        likes[index] = n;
			    });
			    
			    likes.sort(function(a, b){return b-a});

			    $('.temp').html('');  

				var top = likes[0];
			    $( ".case .item" ).each(function( index ) {
			    	$(this).attr('class','');
					$(this).addClass('animated bounceOutUp item');

			    	data = parseInt($(this).find('img').data('likes'));
					if(data==top){
			        	item = $(this).html();
						$('.temp').append('<div class="col span_1_of_3 animated bounceInUp"><div class="item">'+item+'</div></div>');
					}
				});
				break;
			case 'comments':
			   var comments = [];
			   
			    $( ".case .item" ).each(function( index ) {
			        comment = $(this).find('img').data('comments');
			        n = parseInt(comment);
			        comments[index] = n;
			    });
			    
			    comments.sort(function(a, b){return b-a});

			    $('.temp').html('');  

				var top = comments[0];
			    $( ".case .item" ).each(function( index ) {
			    	$(this).attr('class','');
					$(this).addClass('animated zoomOutRight item');

			    	data = parseInt($(this).find('img').data('comments'));
					if(data==top){
			        	item = $(this).html();
						$('.temp').append('<div class="col span_1_of_3 animated zoomInUp"><div class="item">'+item+'</div></div>');
					}
				});
				break;
			case 'effects':
				$('.filter .sub .request').css({ display: 'none' });
				$('.filter .sub .'+v).css({ display: 'block' });		
				
				var h = $('.'+v).height();
				
				$('.filter .sub').animate({
					height: h
				},200);

				$('.filter-request').animate({
					height: 0
				},200);
				
				break;
			default:
		}
		$('.request .list li').click(function() {
		    var effects = [
		        ["zoomInRight", "zoomInDown", "zoomInLeft","zoomInUp"],
		        ["bounceInRight", "bounceInDown", "bounceInLeft","bounceInUp"],
		        ["rotateInDownRight", "rotateInDownLeft", "rotateInUpLeft","rotateInUpRight"]
		    ];
		    var effectsOut = [
		        ["zoomOutRight", "zoomOutDown", "zoomOutLeft","zoomOutUp"],
		        ["bounceOutRight", "bounceOutDown", "bounceOutLeft","bounceOutUp"],
		        ["rotateOutDownRight", "rotateOutDownLeft", "rotateOutUpLeft","rotateOutUpRight"]
		    ];

		    	effects.sort(function() { return 0.5 - Math.random() });
		    	var animated = effects[0];

			var eCurrent = $(this).html();
			var eList    = [];



			$( ".item" ).each(function( index ) {
				var effect = $(this).find('img').data('effects');

				if(effect == eCurrent){
					eList.push('<div class="col span_1_of_3 animated zoomInRight"><div class="item">' + $(this).html() + '</div></div>');
				}

				$(this).addClass('animated zoomOutRight');
			});			

			$('.temp').html('');
			$('.temp').html(eList);

			$('.filter .sub').animate({
				height: 0
			},200);

			$('.filter .sub .request').css({ display: 'none' });
			$('.filter-request').animate({
				height: '24px'
			},200);
		});
	});
});