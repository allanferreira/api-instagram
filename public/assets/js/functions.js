function init(){
    intro();   
}
function plural(number, single){
    if(number == 1){
        return number + " " + single;
    }
    else{
        return number + " " + single + "s";
    }
}
   
function loadFilters(){
    var effects = [];
    $( ".case .item" ).each(function( index ) {
        effect = $( this ).find('img').data('effects');
        effects.push(effect);   
    });
    var uniqueEffects = jQuery.unique(effects);

    for(var i = 0; i < uniqueEffects.length; i++){
        $('.filter .sub .effects .list').append('<li>'+uniqueEffects[i]+'</li>');
    }  
}

function intro(){
    setTimeout(function(){
        $('.blank').hide('fade');
        intagramAPI();
    },1000);
	setTimeout(function(){
       $('.menu').show();
	   $('.info').show('fade');
    },3000);
}
function load(data){
	var k, i = 0, randorder = [];
    var effects = [
        ["zoomInRight", "zoomInDown", "zoomInLeft","zoomInUp"],
        ["bounceInRight", "bounceInDown", "bounceInLeft","bounceInUp"],
        ["rotateInDownRight", "rotateInDownLeft", "rotateInUpLeft","rotateInUpRight"]
    ];

    effects.sort(function() { return 0.5 - Math.random() });
    var animated = effects[0];

    var indexValue = 1, t = data.data.length, index = [];
    for(var o = 0; o < t; o++){
        index.push(indexValue + "0");
        indexValue++;
    }
    
    var lenA = animated.length;
    index.sort(function() { return 0.5 - Math.random() });
    

    for(k in data.data) {    
        rand     = Math.floor(Math.random() * lenA);
        currentA = animated[rand];

        image_link    = data.data[i].link;
        image_url_low = data.data[i].images.low_resolution.url;
        image_id      = data.data[i].id;
        username      = data.data[i].user.username;
        filters       = data.data[i].filter;
        likes         = plural(data.data[i].likes.count, 'curtida');
        comments      = plural(data.data[i].comments.count, 'comentário');

        str =  "<div style='z-index:" + index[i] + "' class='item animated " + currentA + "'>";
        str += "<img data-effects='" + filters + "' data-likes='" + likes + "' data-comments='" + comments + "' alt='" + username + ' - image_' + image_id + "' src='" + image_url_low + "'>";
        str += "<div class='info'>";
        str += "<div class='filters'><span>" + filters + "</span></div>";
        str += "<div class='likes'><span>" + likes + "</span></div>";
        str += "<div class='comments'><span>" + comments + "</span></div>";
        str += "</div></div>";
        randorder.push(str);
        
        i++;
    }
    $('.case').html(randorder);
    setTimeout(loadFilters(),500);
}