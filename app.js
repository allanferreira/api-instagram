'use strict';
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var router = express.Router();
var fs = require('fs');

app.use(express.static('public'));

app.get('/', function(req, res) {
    res.sendfile('./public/index.html');
});

app.listen(5000);